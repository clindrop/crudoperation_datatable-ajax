﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD.Core.Entities
{
    public class Employee
    {
        public int EmployeeID { get; set; }
        [Required (ErrorMessage = "This field is Required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "This field is Required")]
        public string Position { get; set; }
        public string Office { get; set; }
        public int Age { get; set; }
        public int Salary { get; set; }
    }
}
