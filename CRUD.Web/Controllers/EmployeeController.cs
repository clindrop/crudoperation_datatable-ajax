﻿using CRUD.Core.Entities;
using CRUD.Core.ViewModels;
using CRUD.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRUD.Web.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Employee
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult GetData()
        {
            using (CRUDContext db = new CRUDContext())
            {
                List<Employee> empList = db.Employees.ToList<Employee>();
                return Json(new { data = empList }, JsonRequestBehavior.AllowGet);
            }
            
            
        }



        public ActionResult AddOrEdit(int id = 0)
        {
            return View(new Employee());
        }



        [HttpPost]
        public ActionResult AddOrEdit(Employee model)
        {
            using (CRUDContext db = new CRUDContext())
            {
                db.Employees.Add(model);
                db.SaveChanges();
                return Json(new { success = true, message = "Saved Successfully" }, JsonRequestBehavior.AllowGet);
            }
            
        }
            

    }
}