﻿using CRUD.Core.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD.Data.Context
{
    public class CRUDContext : DbContext
    {

        public CRUDContext(): base("CrudOpDB")
        {

        }



        public DbSet<Employee> Employees { get; set; }
    }
}
